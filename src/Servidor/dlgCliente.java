package Servidor;

import java.io.*;
import java.net.*;
import javax.swing.*;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class dlgCliente extends javax.swing.JDialog {

    private PrintStream salida = null;
    private Socket ClienteSocket = null; // Socket 
    private String IP="127.0.0.1"; //Ip, domino, o Nombre de la maquina
    private final int PUERTO = 18432;
    
    
    public dlgCliente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        try {
                       ClienteSocket = new Socket(IP,PUERTO);  
                        salida = new PrintStream(ClienteSocket.getOutputStream());
           } catch (UnknownHostException e) {
               System.err.println( IP +" desconocido");
               System.exit(1);
             }          catch (IOException e) {System.err.println("No se puede establecer conexión"); 
                                 System.exit(1);
                                 }
                 salida.println("\t");
                 salida.println("iniciando");
    }

    private String SelectColor() {
        String color = "";
        if (rb_circulo.isSelected()) {
            if (rb_azul.isSelected()) {
                color = "11";
            } else if (rb_verde.isSelected()) {
                color = "12";
            } else if (rb_rojo.isSelected()) {
                color = "13";
            } else {
                System.out.println("Selecciona un color3");
            }
        } 
        else if (rb_rectangulo.isSelected()) {
            if (rb_azul.isSelected()) {
                color = "21";
            } else if (rb_verde.isSelected()) {
                color = "22";
            } else if (rb_rojo.isSelected()) {
                color = "23";
            } else {
                System.out.println("Selecciona un color2");
            }
        } else {
            System.out.println("Selecciona un color1");
        }
        return color;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg_colores = new javax.swing.ButtonGroup();
        bg_forma = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        rb_circulo = new javax.swing.JRadioButton();
        rb_rectangulo = new javax.swing.JRadioButton();
        rb_azul = new javax.swing.JRadioButton();
        rb_verde = new javax.swing.JRadioButton();
        rb_rojo = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        btnEnviarTarea = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setText("--------------------------------------------------");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(30, 270, 270, 20);

        jLabel2.setText("---------------------Figura---------------------");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(30, 10, 260, 20);

        bg_forma.add(rb_circulo);
        rb_circulo.setText("Círculo");
        getContentPane().add(rb_circulo);
        rb_circulo.setBounds(40, 50, 61, 20);

        bg_forma.add(rb_rectangulo);
        rb_rectangulo.setText("Rectángulo");
        getContentPane().add(rb_rectangulo);
        rb_rectangulo.setBounds(40, 90, 83, 20);

        bg_colores.add(rb_azul);
        rb_azul.setText("Azul");
        getContentPane().add(rb_azul);
        rb_azul.setBounds(30, 160, 46, 20);

        bg_colores.add(rb_verde);
        rb_verde.setText("Verde");
        getContentPane().add(rb_verde);
        rb_verde.setBounds(30, 200, 53, 20);

        bg_colores.add(rb_rojo);
        rb_rojo.setText("Rojo");
        getContentPane().add(rb_rojo);
        rb_rojo.setBounds(30, 240, 47, 20);

        jLabel3.setText("---------------------Colores---------------------");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 130, 270, 20);

        btnEnviarTarea.setText("Enviar Tarea");
        btnEnviarTarea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarTareaActionPerformed(evt);
            }
        });
        getContentPane().add(btnEnviarTarea);
        btnEnviarTarea.setBounds(90, 290, 120, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEnviarTareaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarTareaActionPerformed
        salida.println(SelectColor());
    }//GEN-LAST:event_btnEnviarTareaActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgCliente dialog = new dlgCliente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bg_colores;
    private javax.swing.ButtonGroup bg_forma;
    public javax.swing.JButton btnEnviarTarea;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    public javax.swing.JRadioButton rb_azul;
    public javax.swing.JRadioButton rb_circulo;
    public javax.swing.JRadioButton rb_rectangulo;
    public javax.swing.JRadioButton rb_rojo;
    public javax.swing.JRadioButton rb_verde;
    // End of variables declaration//GEN-END:variables
}
